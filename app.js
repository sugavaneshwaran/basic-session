function validate() {
    var username = document.getElementById("fname").value;
    var email = document.getElementById('Email');
    var mobile = document.getElementById("number").value;
    var companyAddress = document.getElementById("inputAddress").value;
    var password = document.getElementById("pass").value;
    var confirmPassword = document.getElementById("pass2").value;
    
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var mob_regex = /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/;
    

    if (username == null || username == "") {
        alert("Please enter the username.");
        return false;
    }

    if (email.value == "") {
        window.alert("Please enter your email");
        return false;
    }

    if (!filter.test(email.value)) {
        alert('Please provide a valid email address');
        email.focus;
        return false;
    }

    if (mobile == "") {
        window.alert("Please enter your mobile number");
        return false;
    }

    if (!mob_regex.test(mobile)) {
        alert("please enter valid mobile number");
        return false;
    }

    if (companyAddress == null || companyAddress == "") {
        alert("Please enter the name of your company");
        return false;
    }

    if (password == "") {
        window.alert("Please enter your password");
        password.focus();
        return false;
    }

    if (confirmPassword == "") {
        window.alert("Please enter to confirm your password");
        confirmPassword.focus();
        return false;
    }

    if (password != confirmPassword) {
        alert("Entered Password is not matching!! Try Again");
        return false;
    }

    alert('Login successful');

}